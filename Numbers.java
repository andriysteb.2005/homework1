import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Numbers {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();
        System.out.println("Let the game begin!");
        System.out.println("Enter your name: ");
        String name = scanner.nextLine();

        int gameNumber = random.nextInt(101);
        int userNumber;

        int[] guessedNumbers = new int[0];


        System.out.println("Enter number from 0 to 100: ");
        while (true) {
            if (!scanner.hasNextInt()) {
                System.err.println("This is not a number !");
                scanner.next();

            } else {
                userNumber = scanner.nextInt();
                if (userNumber > 100 || userNumber < 0) {
                    System.err.println("Your number is out of the range!");
                } else {
                    int[] newArray = Arrays.copyOf(guessedNumbers, guessedNumbers.length + 1);
                    newArray[guessedNumbers.length] = userNumber;
                    guessedNumbers = newArray;
                    if (userNumber > gameNumber) {
                        System.out.println("Your number is too big. Please, try again.");
                    } else if (userNumber < gameNumber) {
                        System.out.println("Your number is too small. Please, try again.");
                    } else {
                        System.out.println("Congratulations, " + name + '!');
                        break;
                    }
                }
            }

        }
        Arrays.sort(guessedNumbers);
        System.out.println("Your numbers:");
        for (int i = guessedNumbers.length - 1; i >= 0; i--) {
            System.out.print(guessedNumbers[i] + "\t");
        }


    }

}
