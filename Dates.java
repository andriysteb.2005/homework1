import java.util.Random;
import java.util.Scanner;

public class Dates{
    public static void main(String[] args) {

        String[][] historicalEvents = {
                {"1939", "When did the World War II begin?"},
                {"1914", "When did the World War I begin?"},
                {"1969", "When was the first moon landing?"},
                {"1961", "When was the first space flight?"},
                {"1922", "When was the USSR formed?"},
                {"1991", "WWhen did Ukraine become independent?"},
                {"1256", "When was Lviv founded?"}
        };

        Random random = new Random();
        int randomIndex = random.nextInt(historicalEvents.length);

        String year = historicalEvents[randomIndex][0];
        String event = historicalEvents[randomIndex][1];

        System.out.println(event);

        int randomYear = Integer.parseInt(year);
        Scanner scanner = new Scanner(System.in);

        while (true) {
            if (!scanner.hasNextInt()) {
                System.err.println("This is not a number !");
                scanner.next();
            } else {

                int guessedYear = scanner.nextInt();

                if (guessedYear == randomYear) {
                    System.out.println("Congratulations, you won!");
                    break;
                } else if (guessedYear < randomYear) {
                    System.out.println("Event took place later");
                } else {
                    System.out.println("Event took place earlier");
                }
            }

        }
    }
}
